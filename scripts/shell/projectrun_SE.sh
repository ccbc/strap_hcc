#!/bin/bash
dirfull=/var/ccbc/project/Ron_Smits_STRAP/
dirproj=${dirfull}
#/run2/
#/20151018_Second_Run_VCaP2x_PC346C2x/
raw="${dirproj}/raw/"
log=1; ## log better at sample level
appendLog=1;
pjobs=8
pushd ${dirproj} > /dev/null
#
ecommandl=()
listf=($(find -L ${raw} -type f -regex ".*\.f\(ast\)?q\.\(gz\|bz2\)$" ! -regex ".*Undetermined.*" | sort ))
for ((i=0; i < ${#listf[@]}; i++))
do
    j=$i;
    listn1[$j]=${listf[$i]}
    i=$((i+1))
    listn2[$j]=${listf[$i]}
    listn3[$j]=$(basename ${listn1[$j]} | sed -e 's/\..*//')
    logdir=${dirproj}/log/${listn3[$j]}
    mkdir -p ${logdir}
    logfile=${logdir}/${listn3[$j]}_log.txt
    testarg=""
    force="";
    ecommand="/home/hvandewerken/project/RNA_Seq_Test/shell/RNA_Seq_pipeline_SE_Illumina_test_two_files.sh -p ${dirproj} -v${testarg}${force} -n ${listn3[$j]} ${listn1[$j]},${listn2[$j]} "
    if [[ ${log} -eq 1 && ${testarg} != "t" ]]; then # log 
	if [[ ${appendLog} -eq 1 && ${force} != "f" ]]; then # append log
	    sign=">>"
	    else 
	    sign=">"
	fi;
	ecommand="${ecommand} 2${sign}\&1 | tee ${logfile}"
    fi;
    ecommandl[${#ecommandl[@]}]="${ecommand};";
done

for COMMAND in "${ecommandl[@]}"; do 
  # Perform command
    # set -x #echo on
    # echo $COMMAND;
    # set +x #echo off
#    continue;
   echo $COMMAND
    # Check status of command
    if [ $? -ne 0 ]; then
        echo -e "harmen: !!Error with: ${COMMAND}"
        exit 2
    fi
done | parallel -j ${pjobs}
##"runs and doesn't wait and standard out in the end with tee something"
#ecommand="parallel  outdir --jobs ${pjobs} /home/hvandewerken/project/RNA_Seq_Test/shell/RNA_Seq_pipeline_SE_Illumina_test.sh -v -n {1} ::: ${listn1[*]} | tee logfile"
#eval "${ecommand}"
popd > /dev/null
exit;
