#!/bin/bash
pushd /home/hvandewerken/CCBC_projects/Ron_Smits_STRAP/raw/2016-10-31_gz
find . -type f -name "*.bz2" | perl -pi -e 's/\.bz2$//g;' | xargs -n1 -P 9 ../../scripts/shell/rezip.sh 
popd
